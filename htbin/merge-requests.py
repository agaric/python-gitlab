#!/usr/bin/env python3

import gitlab
import config

print("Content-Type: text/html\n")
print("<!doctype html><title>Open merge requests</title><h1>Open Merge Requests</h1>")

gl = gitlab.Gitlab('https://gitlab.com/', private_token=config.private_token)

# projects = gl.projects.list(simple=True, membership=True, with_merge_requests_enabled=True, limit=50)

print("<ul>")

# for project in projects:
project_ids = [
    'agaric/sites/agaric-com',
    'agaric/python-gitlab',
    'nichq/nichq-community',
    'nichq/nichq-data',
    'nichq/nichq-org',
    'portside/portside',
    'project_GUTS/TwiG',
#    'terravoz/findit', # sorry won't work for GitHub repositories
]
for project_id in project_ids:

    try:
        project = gl.projects.get(project_id)

        print("<li>")
        print(project.name_with_namespace)
        print(" (")
        print(project.id)
        print(")")

        try:
            mrs = project.mergerequests.list(state="opened")

            if len(mrs):
                print("<ul>")
                for mr in mrs:
                    print("<li><a href='")
                    print(mr.web_url)
                    print("'>")
                    print(mr.title)
                    print("</a></li>")

                print("</ul>")
            else:
                print(" - No open merge requests")

        except:
            print("<ul><li>GitLab error, probably couldn't get merge requests</li></ul>")

    except:
        print("<li>Gitlab error, probably project not found for ", project_id)

    print("</li>")

print("</ul>")
